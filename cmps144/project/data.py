import csv,pickle,shutil,os
from os import listdir
from os.path import isfile, join

def create_team(columns, data):
    team = {}
    i = 0
    for column in data:
        if i == 0:
            team[columns[i]] = column.lower()
        else:
            team[columns[i]] = column
        i+=1
    return team

def write_team(data):
    with open(data["team"] + ".pickle", 'w') as handle:
        pickle.dump(data, handle, protocol=pickle.HIGHEST_PROTOCOL)

def open_all_teams():
    teams = []
    for team in listdir("teams"):
        if ".pickle" in team:
            with open("teams/" + team, 'rb') as handle:
                teams.append(pickle.load(handle))
    return teams
