import re, nltk, argparse, sys
from nltk.corpus import stopwords
import nltk.collocations
from nltk.collocations import BigramCollocationFinder
from nltk import ConditionalFreqDist

def get_score(review):
    return int(re.search(r'Overall = ([1-5])', review).group(1))

def get_text(review):
    return re.search(r'Text = "(.*)"', review).group(1)

def read_reviews(file_name):
    """
    Dont change this function.

    :param file_name:
    :return:
    """
    file = open(file_name, "rb")
    raw_data = file.read().decode("latin1")
    file.close()

    positive_texts = []
    negative_texts = []
    first_sent = None
    for review in re.split(r'\.\n', raw_data):
        overall_score = get_score(review)
        review_text = get_text(review)
        if overall_score > 3:
            positive_texts.append(review_text)
        elif overall_score < 3:
            negative_texts.append(review_text)
        if first_sent == None:
            sent = nltk.sent_tokenize(review_text)
            if (len(sent) > 0):
                first_sent = sent[0]
    return positive_texts, negative_texts, first_sent


########################################################################
## Dont change the code above here
######################################################################
def write_unigrams(review_collection, category):
    stop_words = set(stopwords.words("english"))
    words = [[word for word in review.split(" ") if (word not in stop_words) and (re.match("^[\w+]*$",word))] for review in review_collection]
    unigrams = []
    for unigram_set in words:
        unigrams.extend(unigram_set)
    unigram_dist = nltk.FreqDist(unigrams)
    output = open(category + "-unigram-freq.txt", "w")
    output = open(category + "-unigram-freq.txt", "a")
    for word,frequency in unigram_dist.most_common():
        output.write(word + " " + str(frequency) + "\n")

def write_bigrams(review_collection, category):
    stop_words = set(stopwords.words("english"))
    output = open(category + "-bigram-freq.txt", "w")
    output = open(category + "-bigram-freq.txt", "a")
    fulltext = ""
    conditions = []
    allbigrams = []
    for review in review_collection:
        review_tokenized = nltk.word_tokenize(review)
        words = [word for word in review_tokenized if (word not in stop_words) and (re.match("^[\w+]*$",word))]
        fulltext = fulltext + " ".join(words) + "\n"
        bigrams = list(nltk.bigrams(words))
        for a,b in bigrams:
            conditions.append(a)
        allbigrams.extend(bigrams)
    cfdist = ConditionalFreqDist(allbigrams)
    fdist = nltk.FreqDist(conditions)
    for condition,frequency in fdist.most_common():
        for word,subf in cfdist[condition].most_common():
            output.write(str(condition) + " " + str(word) + " " + str(subf))
    nltk.Text(nltk.word_tokenize(fulltext)).collocations()


def process_reviews(file_name):

    positive_texts, negative_texts, first_sent = read_reviews(file_name)
    positive_texts = [review.lower() for review in positive_texts]
    negative_texts = [review.lower() for review in negative_texts]
    first_sent = first_sent.lower()
    write_unigrams(positive_texts, "positive")
    write_unigrams(negative_texts, "negative")
    write_bigrams(positive_texts,"positive")
    write_bigrams(negative_texts,"negative")

    # Your code goes here





# Write to File, this function is just for reference, because the encoding matters.
def write_file(file_name, data):
    file = open(file_name, 'w', encoding="utf-8")    # or you can say encoding="latin1"
    file.write(data)
    file.close()


def write_unigram_freq(category, unigrams):
    """
    A function to write the unigrams and their frequencies to file.

    :param category: [string]
    :param unigrams: list of (word, frequency) tuples
    :return:
    """
    uni_file = open("{0}-unigram-freq-n.txt".format(category), 'w', encoding="utf-8")
    for word, count in unigrams:
        uni_file.write("{0:<20s}{1:<d}\n".format(word, count))
    uni_file.close()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Assignment 2')
    parser.add_argument('-f', dest="fname", default="restaurant-training.data",  help='File name.')
    args = parser.parse_args()
    fname = args.fname

    process_reviews(fname)
