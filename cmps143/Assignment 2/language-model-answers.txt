1.
POSITIVE UNIGRAMS
food 92
great 66
place 45
service 41
best 40
one 37
restaurant 36
good 32
excellent 24
wonderful 23
every 21
get 20
worth 20
time 20
menu 20
---
NEGATIVE UNIGRAMS
food 121
us 72
service 67
restaurant 66
one 60
would 53
place 51
table 46
go 45
never 45
get 42
even 41
like 39
told 39
waitress 36
---
POSITIVE BIGRAMS
great food 13
food excellent 11
great place 9
food service 8
highly recommend 8
food great 8
food wonderful 6
wine list 6
food good 6
wonderful service 5
service friendly 5
worth trip 5
go back 5
would recommend 5
every time 5
---
NEGATIVE BIGRAMS
dining experience 12
go back 10
prime rib 8
number one 8
coral grill 8
food cold 7
looked like 6
come back 6
wait staff 6
told us 6
20 minutes 6
much better 6
fried rice 6
sunset restaurant 5
local boys 5
---
2.
POSITIVE COLLOCATIONS
chez capo; highly recommend; pancake house; san francisco; mashed
potatoes; millbrae pancake; wine list; rosa negra; several times;
worth trip; big city; food excellent; sure try; head chef; something
everyone; would recommend; ala carte; eastern market; outdoor patio;
ravioli browned
---
NEGATIVE COLLOCATIONS
prime rib; coral grill; dining experience; fried rice; number one;
crab legs; 227 bistro; taco bell; tourist trap; local boys; needless
say; looked like; health department; speak manager; sunset restaurant;
medium rare; wait staff; pattio area; food cold; come back
---
3.
P(excellent) * P(restaurant|excellent) = 
P(excellent) = 0.005017771273259461
P(restaurant|excellent) = 1 in 18
Given only positive reviews, the likelihood of "excellent restaurant" occurring is 0.0002787650707, or 0.03%
---
4.
P(an excellent restaurant) = P(an) * P(excellent|an) * P(restaurant|an excellent)
This is order 3
---
5.
P(mashed U potatoes) =  P(mashed) + P(potatoes) - (P(mashed)P(potatoes|mashed))
P(mashed) = 0.0014635166213673428
P(potatoes) = 0.0006272214091574326 
P(mashed) * P(potatoes|mashed) = (0.0014635166213673428 * 0.0014635166213673428)
0.0014635166213673428+0.0006272214091574326-0.000002141880901
P(mashed U potatoes) = 0.00208859615 or 0.21%
---
6. If you see a word you've never seen in the training data, you use the probability of words you've seen once to calculate the overall probability.
---
7. I would think that there would be diminishing returns from using high orders above 4 or 5 as the language being processed will continue to include more and more variables, resulting in more single-occurrence phrases, while 2 or 3 order results in more frequent and more predictable data.