# Robert Crosby
# rncrosby@ucsc.edu
# 1529995

#!/usr/bin/env python
from __future__ import division
import nltk, zipfile, argparse, sys, re
from nltk import word_tokenize
from nltk import probability
from nltk.corpus import wordnet as wn

sys.stdout = open("wordnet.txt", "w")
sys.stdout.write("Chosen Word: bird\n")
sys.stdout.write("Synsets:\n")
for synset in wn.synsets("bird"):
    sys.stdout.write(str(synset) + ", Definition: " + synset.definition() + "\n")
sys.stdout.write("- - - - - -\n")
fSynset = wn.synsets("bird")[1]
sys.stdout.write("Hyponyms and Root Hyponyms:\n")
sys.stdout.write(str(fSynset) + ", Hyponyms: " + str(fSynset.hyponyms()) + "\nRoot Hyponyms: " + str(fSynset.root_hypernyms()) + "\n")
pathIndex = 0
sys.stdout.write("- - - - - -\n")
sys.stdout.write("Paths:\n")
for path in fSynset.hypernym_paths():
    sys.stdout.write("Path " + str(pathIndex) + ": ")
    for synset in path:
        sys.stdout.write(synset.name())
    sys.stdout.write("\n")
