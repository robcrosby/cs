import re, nltk, pickle, argparse
import os, sys
import data_helper
from features import *
from nltk import classify
from nltk.metrics import ConfusionMatrix

DATA_DIR = "data"

def pickle_tuples(tuples, name):
    with open(name + ".pkl", 'wb') as file:
        pickle.dump(tuples, file)

def load_pickle_tuple(name):
    with open(name, 'rb') as file:
        classifier = nltk.classify.NaiveBayesClassifier.train(pickle.load(file))
        return classifier

def write_features_category(features_category_tuples, output_file_name):
    output_file = open(output_file_name, "w", encoding="utf-8")
    output_text = ""
    for (features, category) in features_category_tuples:
        output_text = output_text + category + ": "
        for key in features:
            output_text = output_text + key + ":" + str(features[key]) + " "
        output_text = output_text + "\n"
    output_file.write(output_text)
    output_file.close()

def get_classifier(classifier_fname):
    classifier_file = open(classifier_fname, 'rb')
    classifier = pickle.load(classifier_file)
    classifier_file.close()
    return classifier

def evaluate(classifier, features_category_tuples, reference_text, data_set_name=None):

    accuracy = nltk.classify.accuracy(classifier, features_category_tuples)
    features = [example[0] for example in features_category_tuples]
    reference = [example[1] for example in features_category_tuples]
    prediction = classifier.classify_many(features)
    confusion_matrix = nltk.ConfusionMatrix(reference,prediction)
    return accuracy, confusion_matrix


def build_features(data_file, feat_name, save_feats=True, binning=False):
    # read text data

    positive_texts, negative_texts = data_helper.get_reviews(os.path.join(DATA_DIR, data_file))

    category_texts = {"positive": positive_texts, "negative": negative_texts}
    # print(category_texts)
    # build features
    features_category_tuples, texts = get_features_category_tuples(category_texts, feat_name)

    # save features to file

    if save_feats is not None:
        if data_file == "train_examples.tsv":
            write_features_category(features_category_tuples,feat_name + "-training-features.txt")
            classifier_name = feat_name + "-training-classifier"
        elif data_file == "dev_examples.tsv":
            write_features_category(features_category_tuples,feat_name + "-development-features.txt")
            classifier_name = feat_name + "-development-classifier"
        else:
            write_features_category(features_category_tuples,feat_name + "-testing-features.txt")
            classifier_name = feat_name + "-testing-classifier"
    return features_category_tuples, texts, classifier_name

def train_model(datafile, feature_set, save_model=None):
    features_data, texts, classifier_fname = build_features(datafile, feature_set)
    classifier = nltk.classify.NaiveBayesClassifier.train(features_data)
    if save_model is not None:
        pickle_tuples(features_data, classifier_fname)
    return classifier

def train_eval(train_file, feature_set, eval_file=None):

    # train the model
    split_name = "train"
    if train_file == "train_examples.tsv":
        informativeName = feature_set + "-training-informative-features.txt"
    elif train_file == "dev_examples.tsv":
        informativeName = feature_set + "-development-informative-features.txt"
    else:
        informativeName = feature_set + "-testing-informative-features.txt"
    model = train_model(train_file, feature_set, True)
    sys.stdout = open(informativeName, 'w')
    model.show_most_informative_features(20)
    sys.stdout = sys.__stdout__

    # evaluate the model
    if eval_file is not None:
        features_data, texts, classifier_fname = build_features(eval_file, feature_set, binning=False)
        accuracy, cm = evaluate(model, features_data, texts, data_set_name=None)
        print("The accuracy of {} is: {}".format(eval_file, accuracy))
        print("Confusion Matrix:")
        print(str(cm))
    else:
        accuracy = None

    return accuracy

def test_pickled_classifier(pklname):
    model = load_pickle_tuple(pklname)
    full_test_text = data_helper.get_reviews(os.path.join(DATA_DIR, "test.txt"))
    # print(category_texts)
    # build features

    all_texts = []
    output_file = open("restaurant-competition-model-P1-predictions.txt", "w", encoding="utf-8")
    output_text = ""
    t = 1
    for text in full_test_text:
        words, tags = get_words_tags(text)
        output_text = output_text + "Test Review " + str(t) + ": " + model.classify(get_ngram_features(words)) + "\n"
        t+=1
    output_file.write(output_text)
    output_file.close()

def feature_vectors_test_data(feature_set):
        full_test_text = data_helper.get_reviews(os.path.join(DATA_DIR, "test.txt"))
        # t = 1
        features_category_tuples = []
        for text in full_test_text:
            feature_vectors = {}
            words, tags = get_words_tags(text)
            if feature_set is "word_features":
                feature_vectors.update(get_ngram_features(words))
            elif feature_set is "word_pos_features":
                word_pos_features = get_ngram_features(words)
                word_pos_features.update(get_pos_features(tags))
                feature_vectors.update(word_pos_features)
            else:
                word_pos_liwc_features = get_ngram_features(words)
                word_pos_liwc_features.update(get_pos_features(tags))
                word_pos_liwc_features.update(get_liwc_features(words))
                feature_vectors.update(word_pos_liwc_features)
            features_category_tuples.append((feature_vectors, "unknown"))
        return features_category_tuples


def main():


    # changing this to true will train the pickle files, making it false will apply the best model to the test data
    test = True
    if test:
    # add the necessary arguments to the argument parser
        parser = argparse.ArgumentParser(description='Assignment 3')
        parser.add_argument('-d', dest="data_fname", default="train_examples.tsv",
                            help='File name of the testing data.')
        args = parser.parse_args()


        train_data = args.data_fname


        eval_data = "dev_examples.tsv"


        for feat_set in ["word_features", "word_pos_features", "word_pos_liwc_features"]:
            print("\nTraining with {}".format(feat_set))
            acc = train_eval(train_data, feat_set, eval_file=eval_data)
    else:
        # these create the feature set files for the data with an unknown category
        write_features_category(feature_vectors_test_data("word_features"),"word_features-testing-features.txt")
        write_features_category(feature_vectors_test_data("word_pos_features"),"word_pos_features-testing-features.txt")
        write_features_category(feature_vectors_test_data("word_pos_liwc_features"),"word_pos_liwc_features-testing-features.txt")
        # this tests the best classifier against the test data
        test_pickled_classifier("word_features-training-classifier.pkl")





if __name__ == "__main__":
    main()
