The accuracy of dev_examples.tsv is: 0.75
Confusion Matrix:
         |   n   p |
         |   e   o |
         |   g   s |
         |   a   i |
         |   t   t |
         |   i   i |
         |   v   v |
         |   e   e |
---------+---------+
negative |<133>  7 |
positive |  63 <77>|
---------+---------+
(row = reference; col = test)