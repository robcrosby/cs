The accuracy of dev_examples.tsv is: 0.85
Confusion Matrix:
         |   n   p |
         |   e   o |
         |   g   s |
         |   a   i |
         |   t   t |
         |   i   i |
         |   v   v |
         |   e   e |
---------+---------+
negative |<264> 16 |
positive |  68<212>|
---------+---------+
(row = reference; col = test)