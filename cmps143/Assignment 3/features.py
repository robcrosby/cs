
import nltk
from nltk.corpus import stopwords
from nltk.util import ngrams
import re
import word_category_counter
import data_helper
import os, sys
import math

DATA_DIR = "data"
LIWC_DIR = "liwc"

word_category_counter.load_dictionary(LIWC_DIR)

def normalize(token, should_normalize=True):
    """
    This function performs text normalization.

    If should_normalize is False then we return the original token unchanged.
    Otherwise, we return a normalized version of the token, or None.

    For some tokens (like stopwords) we might not want to keep the token. In
    this case we return None.

    :param token: str: the word to normalize
    :param should_normalize: bool
    :return: None or str
    """
    if not should_normalize:
        return token
    else:
        stop_words = set(stopwords.words("english"))
        if (re.match("^[\w+]*$",token)) and (token not in stop_words):
            return token.lower()
        else:
            return None
        raise NotImplemented


def get_words_tags(text, should_normalize=True):
    """
    This function performs part of speech tagging and extracts the words
    from the review text.

    You need to :
        - tokenize the text into sentences
        - word tokenize each sentence
        - part of speech tag the words of each sentence

    Return a list containing all the words of the review and another list
    containing all the part-of-speech tags for those words.

    :param text:
    :param should_normalize:
    :return:
    """
    words = []
    tags = []

    # tokenization for each sentence
    sentences = nltk.sent_tokenize(text)
    for sentence in sentences:
        tokens = nltk.word_tokenize(sentence)
        pos = nltk.pos_tag(tokens)
        if should_normalize:
            for word,tag in pos:
                normalized = normalize(word)
                if normalized is not None:
                    words.append(normalized)
                    tags.append(tag)
        else:
            for word,tag in pos:
                    words.append(word)
                    tags.append(tag)
    return words, tags


def get_ngram_features(tokens):
    """
    This function creates the unigram and bigram features as described in
    the assignment3 handout.

    :param tokens:
    :return: feature_vectors: a dictionary values for each ngram feature
    """

    feature_vectors = {}
    unigrams = tokens
    bigrams = list(ngrams(tokens,2))
    for unigram in unigrams:
        f_unigram = ("UNI_" + unigram)
        if f_unigram in feature_vectors:
            feature_vectors[f_unigram] = bin(feature_vectors[f_unigram]+1)
        else:
            feature_vectors[f_unigram] = bin(1)
    for bigram in bigrams:
        f_bigram = ("BIGRAM_" + bigram[0] + "_" + bigram[1])
        if f_bigram in feature_vectors:
            feature_vectors[f_bigram] = bin(feature_vectors[f_bigram]+1)
        else:
            feature_vectors[f_bigram] = bin(1)
    return feature_vectors

def bin(count):
    """
    Results in bins of  0, 1, 2, 3 >=
    :param count: [int] the bin label
    :return:
    """
    return count if count < 2 else 3

def get_pos_features(tags):
    """
    This function creates the unigram and bigram part-of-speech features
    as described in the assignment3 handout.

    :param tags: list of POS tags
    :return: feature_vectors: a dictionary values for each ngram-pos feature
    """
    unigrams = tags
    bigrams = list(ngrams(tags,2))
    feature_vectors = {}
    for unigram in unigrams:
        f_unigram = ("UNI_" + unigram)
        if f_unigram in feature_vectors:
            feature_vectors[f_unigram] = bin(feature_vectors[f_unigram]+1)
        else:
            feature_vectors[f_unigram] = bin(1)
    for bigram in bigrams:
        f_bigram = ("BIGRAM_" + bigram[0] + "_" + bigram[1])
        if f_bigram in feature_vectors:
            feature_vectors[f_bigram] = bin(feature_vectors[f_bigram]+1)
        else:
            feature_vectors[f_bigram] = bin(1)
    return feature_vectors

def get_liwc_features(words):
    """
    Adds a simple LIWC derived feature

    :param words:
    :return:
    """

    # TODO: binning

    feature_vectors = {}
    text = " ".join(words)
    liwc_scores = word_category_counter.score_text(text)
    for key in liwc_scores:
        if key not in {'Word Count', 'Sentences','Words Per Sentence', 'Newlines', 'Total Function Words', 'Total Pronouns','Negative Emotion','Positive Emotion'}:
            feature_vectors["liwc:" + (key.replace(" ","_").lower())] = 0 if liwc_scores[key] <= 1 else liwc_scores[key]
        else:
            feature_vectors["liwc:" + (key.replace(" ","_").lower())] = liwc_scores[key]
    # All possible keys to the scores start on line 269
    # of the word_category_counter.py script
    negative_score = liwc_scores["Negative Emotion"]
    positive_score = liwc_scores["Positive Emotion"]
    feature_vectors["Negative Emotion"] = negative_score
    feature_vectors["Positive Emotion"] = positive_score
    if positive_score > negative_score:
        feature_vectors["liwc:positive"] = 1
    else:
        feature_vectors["liwc:negative"] = 1
    return feature_vectors


FEATURE_SETS = {"word_pos_features", "word_features", "word_pos_liwc_features"}

def get_features_category_tuples(category_text_dict, feature_set):
    """

    You will might want to update the code here for the competition part.

    :param category_text_dict:
    :param feature_set:
    :return:
    """
    features_category_tuples = []
    all_texts = []

    assert feature_set in FEATURE_SETS, "unrecognized feature set:{}, Accepted values:{}".format(feature_set, FEATURE_SETS)
    for category in category_text_dict:
        for text in category_text_dict[category]:
            feature_vectors = {}
            words, tags = get_words_tags(text)
            if feature_set is "word_features":
                feature_vectors.update(get_ngram_features(words))
            elif feature_set is "word_pos_features":
                word_pos_features = get_ngram_features(words)
                word_pos_features.update(get_pos_features(tags))
                feature_vectors.update(word_pos_features)
            else:
                word_pos_liwc_features = get_ngram_features(words)
                word_pos_liwc_features.update(get_pos_features(tags))
                word_pos_liwc_features.update(get_liwc_features(words))
                feature_vectors.update(word_pos_liwc_features)
            features_category_tuples.append((feature_vectors, category))
            all_texts.append(text)
    return features_category_tuples, all_texts

"""
Below method was taken from this stackoverflow answer: https://stackoverflow.com/a/34325723
I use this as a simple way to track progreess of long running tasks and am not taking credit for
someone elses work.
"""
def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█'):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = '\r')
    # Print New Line on Complete
    if iteration == total:
        print()



if __name__ == "__main__":
    # pos,neg = data_helper.get_reviews("data/train_examples.tsv")
    # print("hi")
    pass
