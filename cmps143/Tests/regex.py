# Robert Crosby
# rncrosby@ucsc.edu
# 1529995

#!/usr/bin/env python
from __future__ import division
import nltk, zipfile, argparse, sys, re
from nltk import word_tokenize
from nltk import probability
from nltk.corpus import wordnet as wn

story = "The/DT Donkey/NNP and/CC the/DT Mule/NNP A/NNP MULETEER/NNP set/VBD forth/NN on/IN a/DT journey/NN ,/, driving/VBG before/IN him/PRP a/DT Donkey/NNP and/CC a/DT Mule/NNP ,/, both/DT well/RB laden/NN ./. The/DT Donkey/NNP ,/, as/RB long/RB as/IN he/PRP traveled/VBD along/RP the/DT plain/NN ,/, carried/VBD his/PRP$ load/NN with/IN ease/NN ,/, but/CC when/WRB he/PRP began/VBD to/TO ascend/VB the/DT steep/JJ path/NN of/IN the/DT mountain/NN ,/, felt/VBD his/PRP$ load/NN to/TO be/VB more/JJR than/IN he/PRP could/MD bear/VB ./. He/PRP entreated/VBD his/PRP$ companion/NN to/TO relieve/VB him/PRP of/IN a/DT small/JJ portion/NN ,/, that/IN he/PRP might/MD carry/VB home/NN the/DT rest/NN ;/: but/CC the/DT Mule/NNP paid/VBD no/DT attention/NN to/TO the/DT request/NN ./. The/DT Donkey/NNP shortly/RB afterwards/VBZ fell/VBD down/RB dead/JJ under/IN his/PRP$ burden/NN ./. Not/RB knowing/VBG what/WP else/RB to/TO do/VB in/IN so/RB wild/JJ a/DT region/NN ,/, the/DT Muleteer/NNP placed/VBD upon/IN the/DT Mule/NNP the/DT load/NN carried/VBN by/IN the/DT Donkey/NNP in/IN addition/NN to/TO his/PRP$ own/JJ ,/, and/CC at/IN the/DT top/NN of/IN all/DT placed/VBN the/DT hide/NN of/IN the/DT Donkey/NNP ,/, after/IN he/PRP had/VBD skinned/VBN him/PRP ./. The/DT Mule/NNP ,/, groaning/VBG beneath/IN his/PRP$ heavy/JJ burden/NN ,/, said/VBD to/TO himself/PRP :/: ``/`` I/PRP am/VBP treated/VBN according/VBG to/TO my/PRP$ deserts/NNS ./. If/IN I/PRP had/VBD only/RB been/VBN willing/JJ to/TO assist/VB the/DT Donkey/NNP a/DT little/JJ in/IN his/PRP$ need/NN ,/, I/PRP should/MD not/RB now/RB be/VB bearing/VBG ,/, together/RB with/IN his/PRP$ burden/NN ,/, himself/PRP as/RB well/RB ./. ''/'' "
noun_phrases = []
for match in re.finditer("(([A-Za-z]*\/(DT|JJ))\W?[A-Za-z]*\/(NNS|NNP|NN)(\W?[A-Za-z]*\/(NNS|NNP|NN))?(\W?[A-Za-z]*\/(NNS|NNP|NN))?)", story):
    print(match.group(1))
    # simple = re.findall("[A-Za-z,.]+(?=\/)", m)
    # phrase = ""
    # for i in simple:
    #     print(str(i))
