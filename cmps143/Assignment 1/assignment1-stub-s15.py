# Robert Crosby
# rncrosby@ucsc.edu
# 1529995

#!/usr/bin/env python
from __future__ import division
import nltk, zipfile, argparse, sys, re
###############################################################################
## Utility Functions ##########################################################
###############################################################################
# This method takes the path to a zip archive.
# It first creates a ZipFile object.
# Using a list comprehension it creates a list where each element contains
# the raw text of the fable file.
# We iterate over each named file in the archive:
#     for fn in zip_archive.namelist()
# For each file that ends with '.txt' we open the file in read only
# mode:
#     zip_archive.open(fn, 'rU')
# Finally, we read the raw contents of the file:
#     zip_archive.open(fn, 'rU').read()
def unzip_corpus(input_file):
    zip_archive = zipfile.ZipFile(input_file)
    try:
        contents = [zip_archive.open(fn, 'rU').read().decode('utf-8')
                for fn in zip_archive.namelist() if fn.endswith(".txt")]
    except ValueError as e:
        contents = [zip_archive.open(fn, 'r').read().decode('utf-8')
                for fn in zip_archive.namelist() if fn.endswith(".txt")]
    return contents

###############################################################################
## Stub Functions #############################################################
###############################################################################
def process_corpus(corpus_name):
    #### open files and initalized lists and counters
    sys.stdout.write("Corpus Name: " + corpus_name + "\n")
    input_file = corpus_name + ".zip"
    corpus_contents = unzip_corpus(input_file)
    wordCount = 0
    posFile = open(corpus_name + "-pos.txt", "w")
    freqFile = open(corpus_name + "-word-freq.txt", "w")
    posOutputText = ""
    freqOutputText = ""
    pattern = "\W"
    fullCorpus = []
    fullCorpusTags = []
    cfdist = nltk.ConditionalFreqDist()
    fullPos = []
    nnwords = []
    vbdwords = []
    jjwords = []
    rbwords = []
    #### end open ####
    # iterate over the files of the corpus
    for i in corpus_contents:
        # tokenize the sentences
        sentences = nltk.sent_tokenize(i)
        # iterate each sentence
        for sentence in sentences:
            # tokenize, store and tag each word
            tokens = nltk.word_tokenize(sentence)
            fullCorpus.extend(tokens)
            pos = nltk.pos_tag(tokens)
            # write tokens, sort by pos
            for key, value in pos:
                fullCorpusTags.append(value)
                posOutputText = posOutputText + (key + "/" + value + " ")
                if value == "NN":
                    nnwords.append(key.lower())
                if value == "VBD":
                    vbdwords.append(key.lower())
                if value == "JJ":
                    jjwords.append(key.lower())
                if value == "RB":
                    rbwords.append(key.lower())
                # increase conditional frequency
                cfdist[key.lower()][value] += 1
                # check if its a word, increase word count
                if not re.findall("\W", key):
                    wordCount+=1
        # seperate each corpus by new line
        posOutputText = posOutputText + ("\n\n")
    # print word count
    sys.stdout.write("Total words in the corpus: " + str(wordCount) + "\n")
    # write the outpput tagged text to file
    posFile.write(posOutputText)
    # lowercase all the words
    fullCorpus = [word.lower() for word in fullCorpus]
    # remove punctuation
    fullCorpus = [word for word in fullCorpus if ((len(word) > 1) or (word == "i")  or (word == "a"))]
    # calculate vocabulary size
    corpusFreqDist = nltk.FreqDist(fullCorpus)
    vocabularySize = len(corpusFreqDist)
    for key,value in corpusFreqDist.most_common():
        freqOutputText = freqOutputText + key + "\n"
    # write result to file
    freqFile.write(freqOutputText)
    sys.stdout.write("Vocabulary Size of the corpus: " + str(vocabularySize))
    # most frequent tag
    for key, value in nltk.FreqDist(fullCorpusTags).most_common(1):
        sys.stdout.write("The most frequent part-of-speech tag is " + str(key) + " with frequency " + str(value))
    # create text file to find common pos words
    fullText = nltk.Text(fullCorpus)
    mostCommonNN = nltk.FreqDist(nnwords).most_common(1)
    mostCommonVBD = nltk.FreqDist(vbdwords).most_common(1)
    mostCommonJJ = nltk.FreqDist(jjwords).most_common(1)
    mostCommonRB = nltk.FreqDist(rbwords).most_common(1)
    sys.stdout.write("\n")
    sys.stdout.write("The most frequent word in the POS NN is " + mostCommonNN[0][0] + " and it's similar words are: ")
    fullText.similar(mostCommonNN[0][0])
    sys.stdout.write("\n")
    sys.stdout.write("The most frequent word in the POS VBD is " + mostCommonVBD[0][0] + " and it's similar words are: ")
    fullText.similar(mostCommonVBD[0][0])
    sys.stdout.write("\n")
    sys.stdout.write("The most frequent word in the POS JJ is " + mostCommonJJ[0][0] + " and it's similar words are: ")
    fullText.similar(mostCommonJJ[0][0])
    sys.stdout.write("\n")
    sys.stdout.write("The most frequent word in the POS RB is " + mostCommonRB[0][0] + " and it's similar words are: ")
    fullText.similar(mostCommonRB[0][0])
    sys.stdout.write("\n")
    # print collocations
    sys.stdout.write("Collocations: ")
    fullText.collocations()
    # reroute output to file for tabulate function
    sys.stdout = open(corpus_name + "-pos-word-freq.txt", "w")
    cfdist.tabulate()
###############################################################################
## Program Entry Point ########################################################
###############################################################################
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Assignment 1')
    parser.add_argument('--corpus', required=True, dest="corpus", metavar='NAME',
                        help='Which corpus to process {fables, blogs}')

    args = parser.parse_args()

    corpus_name = args.corpus

    if corpus_name == "fables" or "blogs":
        process_corpus(corpus_name)
    else:
        print("Unknown corpus name: {0}".format(corpus_name))
