import nltk
from nltk.corpus import stopwords
from nltk.util import ngrams
from nltk.metrics import ConfusionMatrix
import re, os, sys, random, pickle
import data_helper
from nltk.classify.scikitlearn import SklearnClassifier
from sklearn.naive_bayes import BernoulliNB
from sklearn.tree import DecisionTreeClassifier
from sklearn import svm
from sklearn import metrics
import features
from word2vec_extractor import Word2vecExtractor

def pickle_tuples(tuples, name):
    with open(name + ".pkl", 'wb') as file:
        pickle.dump(tuples, file)

def load_pickle_tuple(name):
    with open(name, 'rb') as file:
        return pickle.load(file)

def load_pickle_tuple_as_classifier(name):
    with open(name, 'rb') as file:
        classifier = nltk.classify.NaiveBayesClassifier.train(pickle.load(file))
        return classifier

def normalize(token, should_normalize=True):
    """
    This function performs text normalization.

    If should_normalize is False then we return the original token unchanged.
    Otherwise, we return a normalized version of the token, or None.

    For some tokens (like stopwords) we might not want to keep the token. In
    this case we return None.

    :param token: str: the word to normalize
    :param should_normalize: bool
    :return: None or str
    """
    if not should_normalize:
        return token
    else:
        stop_words = set(stopwords.words("english"))
        if (re.match("^[\w+]*$",token)) and (token not in stop_words):
            return token.lower()
        else:
            return None
        raise NotImplemented


def get_words_tags(text, should_normalize=True):
    """
    This function performs part of speech tagging and extracts the words
    from the review text.

    You need to :
        - tokenize the text into sentences
        - word tokenize each sentence
        - part of speech tag the words of each sentence

    Return a list containing all the words of the review and another list
    containing all the part-of-speech tags for those words.

    :param text:
    :param should_normalize:
    :return:
    """
    words = []
    tags = []

    # tokenization for each sentence
    sentences = nltk.sent_tokenize(text)
    for sentence in sentences:
        tokens = nltk.word_tokenize(sentence)
        pos = nltk.pos_tag(tokens)
        if should_normalize:
            for word,tag in pos:
                normalized = normalize(word)
                if normalized is not None:
                    words.append(normalized)
                    tags.append(tag)
        else:
            for word,tag in pos:
                    words.append(word)
                    tags.append(tag)
    return words, tags

def get_ngram_features(tokens, binning=False):
    """
    This function creates the unigram and bigram features as described in
    the assignment3 handout.

    :param tokens:
    :return: feature_vectors: a dictionary values for each ngram feature
    """

    feature_vectors = {}
    unigrams = tokens
    bigrams = list(ngrams(tokens,2))
    for unigram in unigrams:
        f_unigram = ("UNI_" + unigram)
        b_value = 0
        if f_unigram in feature_vectors:
            if binning:
                b_value = bin(feature_vectors[f_unigram]+1)
            else:
                b_value = feature_vectors[f_unigram]+1
        else:
            if binning:
                b_value = bin(1)
            else:
                b_value = 1
    for bigram in bigrams:
        f_bigram = ("BIGRAM_" + bigram[0] + "_" + bigram[1])
        b_value = 0
        if f_bigram in feature_vectors:
            if binning:
                b_value = bin(feature_vectors[f_bigram]+1)
            else:
                b_value = feature_vectors[f_bigram]+1
        else:
            if binning:
                b_value = bin(1)
            else:
                b_value = 1
    return feature_vectors

def get_features_category_tuples(category_text_dict, bin=False):
    features_category_tuples = []
    # all_texts = []

    for category in category_text_dict:
        for text in category_text_dict[category]:
            words, tags = get_words_tags(text)
            features_category_tuples.append((get_ngram_features(words,bin), category))
            # all_texts.append(text)
    return features_category_tuples


def bin(count):
    return count if count < 2 else 3

def bin_label(vector, label, restrict=True):
    if label in vector:
        if restrict:
            if vector[label] < 2:
                vector[label] = vector[label] + 1
            else:
                vector[label] = 10
        else:
            vector[label] = vector[label]+1
    else:
        vector[label] = 1


def build_vector(tokens, features, bin=True):
    unigrams = tokens
    bigrams = list(ngrams(tokens,2))
    vector = {}
    for unigram in unigrams:
        # label = "UNI_" + unigram
        label = unigram
        if features is None or label in features:
            bin_label(vector,label, bin)
    for bigram in bigrams:
        label = bigram[0] + " " + bigram[1]
        # label = "BI_" + bigram[0] + "_" + bigram[1]
        if features is None or label in features:
            bin_label(vector,label, bin)
    if not vector:
        return None
    return vector

def build_filtered_tuple(filename, including_features, bin=True):
    positive, negative = data_helper.get_reviews(filename)
    tuples = []
    for text in positive:
        words, tags = get_words_tags(text)
        vector = build_vector(words,including_features, bin)
        if vector:
            tuples.append((vector,"positive"))
    for text in negative:
        words, tags = get_words_tags(text)
        vector = build_vector(words,including_features, bin)
        if vector:
            tuples.append((vector,"negative"))
    return tuples

def classify_accuracy_cm(classifier, test, matrix=False):
    accuracy = nltk.classify.accuracy(classifier, test)
    features = [example[0] for example in test]
    reference = [example[1] for example in test]
    prediction = classifier.classify_many(features)
    if matrix:
        return accuracy, nltk.ConfusionMatrix(reference,prediction)
    else:
        return accuracy

def hw_one_one(input_file, output_file):
    bin_train_data = build_filtered_tuple(input_file, None, True)
    train_data = build_filtered_tuple(input_file, None, False)
    test_data = build_filtered_tuple("test_examples.tsv", None)
    bin_classifier = nltk.classify.NaiveBayesClassifier.train(bin_train_data)
    classifier = nltk.classify.NaiveBayesClassifier.train(train_data)
    bin_accuracy, bin_matrix = classify_accuracy_cm(bin_classifier, test_data, True)
    accuracy, matrix = classify_accuracy_cm(classifier, test_data, True)
    f = open(output_file, "w")
    out = "Bin Accuracy: " + str(bin_accuracy) + "\n" + str(bin_matrix) + "\nNo Bin Accuracy: " + str(accuracy) + "\n" + str(matrix)
    f.write(out)
    f.close()

def hw_one_one_one():
    train_data = build_filtered_tuple("train_examples.tsv", None)
    develop_data = build_filtered_tuple("dev_examples.tsv", None)
    classifier = nltk.NaiveBayesClassifier.train(train_data)
    best_features = classifier.most_informative_features(10000)
    best = (0.0, 0)
    for i in [2**i for i in range(5, 15)]:
        selected_features = set([fname for fname, value in best_features[:i]])
        train_data = build_filtered_tuple("train_examples.tsv",selected_features)
        accuracy = classify_accuracy_cm(nltk.NaiveBayesClassifier.train(train_data),develop_data)
        print("{0:6d} {1:8.5f}".format(i, accuracy))
        if accuracy > best[0]:
            best = (accuracy, i)
    # Now train on the best features
    selected_features = set([fname for fname, value in best_features[:best[1]]])
    test_data = build_filtered_tuple("test_examples.tsv", None)
    filtered_train = build_filtered_tuple("train_examples.tsv",selected_features)
    accuracy = classify_accuracy_cm(nltk.NaiveBayesClassifier.train(filtered_train),test_data)
    print("{0:6s} {1:8.5f}".format("Test", accuracy))
    pickle_tuples(filtered_train,"selected_features")
    print("Stored Selected Features: selected_features.pkl")

def hw_two():
    develop_data = build_filtered_tuple("dev_examples.tsv", None)
    test_data = build_filtered_tuple("test_examples.tsv", None)
    train_data = load_pickle_tuple("selected_features.pkl")
    classifier = nltk.NaiveBayesClassifier.train(train_data)
    NBNLTKDEV_accuracy = classify_accuracy_cm(classifier, develop_data)
    NBNLTKTEST_accuracy = classify_accuracy_cm(classifier, test_data)
    BNB_classifier = SklearnClassifier(BernoulliNB())
    BNB_classifier.train(train_data)
    DT_classifier = SklearnClassifier(DecisionTreeClassifier())
    DT_classifier.train(train_data)
    BNBDEV_accuracy = classify_accuracy_cm(BNB_classifier, develop_data)
    BNBTEST_accuracy = classify_accuracy_cm(BNB_classifier, test_data)
    DTDEV_accuracy = classify_accuracy_cm(DT_classifier, develop_data)
    DTTEST_accuracy = classify_accuracy_cm(DT_classifier, test_data)
    print("NB NLTK ON DEV " + str(NBNLTKDEV_accuracy))
    print("NB NLTK ON TEST " + str(NBNLTKTEST_accuracy))
    print("BNB ON DEV " + str(BNBDEV_accuracy))
    print("BNB ON TEST " + str(BNBTEST_accuracy))
    print("DT ON DEV " + str(DTDEV_accuracy))
    print("DT ON TEST " + str(DTTEST_accuracy))

def svm_tuples(filename):
    positive, negative = data_helper.get_reviews(filename)
    w2vecmodel = "data/glove-w2v.txt"
    w2v = Word2vecExtractor(w2vecmodel)
    tuples = []
    for review in positive:
            tuples.append((w2v.get_doc2vec_feature_dict(review),"positive"))
    for review in negative:
        tuples.append((w2v.get_doc2vec_feature_dict(review),"negative"))
    x = [[tuple[0][key] for key in tuple[0]] for tuple in tuples]
    y = [tuple[1] for tuple in tuples]
    return x,y

def hw_two_two():
    # train_data = load_pickle_tuple("selected_features.pkl")
    # train_data = build_filtered_tuple("train_examples.tsv", None)
    x_train, y_train = svm_tuples("train_examples.tsv")
    x_dev, y_dev = svm_tuples("dev_examples.tsv")
    x_test, y_test = svm_tuples("test_examples.tsv")
    svm_classifier = svm.SVC(kernel='linear') # Linear Kernel
    svm_classifier.fit(x_train, y_train)
    y_dev_pred = svm_classifier.predict(x_dev)
    y_test_pred = svm_classifier.predict(x_test)
    print("Development Accuracy:",metrics.accuracy_score(y_dev, y_dev_pred))
    print("Test Accuracy:",metrics.accuracy_score(y_test, y_test_pred))

def hw_two_two_filtered():
    train_data = load_pickle_tuple("selected_features.pkl")
    w2vecmodel = "data/glove-w2v.txt"
    w2v = Word2vecExtractor(w2vecmodel)
    tuples = []
    for tuple in train_data:
        sentence = ""
        for key in tuple[0]:
            sentence = sentence + key + " "
        sentence = sentence[:-1] + "."
        tuples.append((w2v.get_doc2vec_feature_dict(sentence),tuple[1]))
    x_train = [[tuple[0][key] for key in tuple[0]] for tuple in tuples]
    y_train = [tuple[1] for tuple in tuples]
    x_dev, y_dev = svm_tuples("dev_examples.tsv")
    x_test, y_test = svm_tuples("test_examples.tsv")
    svm_classifier = svm.SVC(kernel='linear') # Linear Kernel
    svm_classifier.fit(x_train, y_train)
    y_dev_pred = svm_classifier.predict(x_dev)
    y_test_pred = svm_classifier.predict(x_test)
    print("Development Accuracy:",metrics.accuracy_score(y_dev, y_dev_pred))
    print("Test Accuracy:",metrics.accuracy_score(y_test, y_test_pred))


def predict(in_file, out_file, nb=False):
    out = ""
    if nb:
        classifier = load_pickle_tuple_as_classifier("selected_features.pkl")
        dev_tuple = build_filtered_tuple("dev_examples.tsv", None)
        test_tuple = build_filtered_tuple("test_examples.tsv", None)
        dev_accuracy, dev_matrix = classify_accuracy_cm(classifier, dev_tuple, True)
        test_accuracy, test_matrix = classify_accuracy_cm(classifier, test_tuple, True)
        out = "Using the NB NLTK Classifier\n\nDev Accuracy: " + str(dev_accuracy) + "\n\nDev Matrix:\n" + str(dev_matrix) + "\n\nTest Accuracy: " + str(test_accuracy) + "\n\nTest Matrix:\n" + str(test_matrix)
        f = open(out_file, "w")
        f.write(out)
        f.close()
    else:
        train_data = load_pickle_tuple("selected_features.pkl")
        w2vecmodel = "data/glove-w2v.txt"
        w2v = Word2vecExtractor(w2vecmodel)
        tuples = []
        for tuple in train_data:
            sentence = ""
            for key in tuple[0]:
                sentence = sentence + key + " "
            sentence = sentence[:-1] + "."
            tuples.append((w2v.get_doc2vec_feature_dict(sentence),tuple[1]))
        x_train = [[tuple[0][key] for key in tuple[0]] for tuple in tuples]
        y_train = [tuple[1] for tuple in tuples]
        x_test, y_test = svm_tuples(in_file)
        svm_classifier = svm.SVC(kernel='linear') # Linear Kernel
        svm_classifier.fit(x_train, y_train)
        y_test_pred = svm_classifier.predict(x_test)
        accuracy = metrics.accuracy_score(y_test,y_test_pred)
        matrix = metrics.confusion_matrix(y_test,y_test_pred)
        f = open(out_file, "w")
        out = "Using the SVM Classifier\n\nAccuracy: " + str(accuracy) + "\n\nMatrix:\n" + str(matrix)
        f.write(out)
        f.close()


def main():
    args = sys.argv
    if len(args) >= 2:
        part = args[1]
        if part == "three":
            if len(args) == 4:
                predict(args[2],args[3])
            else:
                print("Please use the following format: python restaurant-competition-P2.py three input_file output_file")
        elif part == "two":
            hw_two()
            hw_two_two()
            hw_two_two_filtered()
        elif part == "one":
            if len(args) == 4:
                hw_one_one(args[2],args[3])
                # hw_one_one_one()
            else:
                print("Please use the following format: python restaurant-competition-P2.py one input_file output_file")
        else:
            print("Please use the following format: python restaurant-competition-P2.py section input_file output_file")

if __name__ == "__main__":
    main()
